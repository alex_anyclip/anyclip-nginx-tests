package com.anyclip.events;

import com.google.gson.JsonObject;

/**
 * Created by alexk on 7/26/2017.
 */
public class Flows {
    public Flows(){

    }

    public JsonObject getWlos(String domainWithPort) {
        String url = domainWithPort + "/anyclip-banker/api/debug/wloEvents";
        return TestUtils.INSTANCE.apiRequest(url);
    }
}
