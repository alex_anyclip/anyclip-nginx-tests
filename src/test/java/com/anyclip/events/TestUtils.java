package com.anyclip.events;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

/**
 * Created by alexk on 7/25/2017.
 */
public enum TestUtils {

    INSTANCE;

    private RequestSpecification headers =
            given()
                .contentType("application/json")
                .header("X-Rtbkit-Protocol-Version","2.2")
                .header("X-Rtbkit-Timestamp", "21739172319")
                .header("X-Rtbkit-Auction-Id", "4725d328edf011e49ebd11250708394f")
                .header("X-Rtbkit-Banker-Name", "anyclipbanker")
                .header("Cache-Control", "no-cache")
                .header("Content-Type", "application/json");


    public JsonObject apiRequest(String url) {
        ValidatableResponse response = this.headers
                .when()
                .get(url)
                .then();

        Gson gson = new Gson();
        String json = response
                .statusCode(200)
                .extract()
                .body()
                .asString();
        JsonObject map = gson.fromJson(json, JsonObject.class);
        return map;
    }
}
