package com.anyclip.events;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.anyclip.S3EventsConnector.eventFileGetters.FileListing;
import com.anyclip.S3EventsConnector.utils.UTCDateTime;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;

import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by alexk on 7/12/2017.
 */
public class EventCountTest {

    private final Logger logger = org.slf4j.LoggerFactory.getLogger(EventCountTest.class);
    private final Flows flows = new Flows();

    @Test
    public void HowManyWinNotifications() throws IOException {
        int expectedEvents = Integer.valueOf(System.getProperty("expectedEvents"));
        int lookBackInMinutes = Integer.valueOf(System.getProperty("lookBackInMinutes"));
        double eventSize = 5.88;
        String bucket = "anyclip-filter-server-qa";
        LocalDateTime now = UTCDateTime.now() ;
        String path = "events/2017/07";
        String datePattern = "\\.(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}).(\\d{2})";
        List<S3ObjectSummary> files = FileListing.INSTANCE.listFiles(now.minus(Duration.ofMinutes(lookBackInMinutes)), now, bucket, path, datePattern);
        int size= 0;
        logger.info("Calculated files");
        for (S3ObjectSummary f : files) {
            logger.info(f.getKey() + " - " + f.getSize());
            size += f.getSize();
        }
        logger.info("Events calculated according to " + eventSize + "byte per each event");
        double actualEvents = size / eventSize;
        Assert.assertEquals(expectedEvents, actualEvents, 5);
    }

    @Test
    public void HowManyWlo() throws IOException {
        int expectedEvents = 0;
        String url = "http://qa2.rtbkit-banker.us-east-1.anyclipsrv.info:8080";
        JsonObject wlos = this.flows.getWlos(url);
        int actualEvents = wlos.get("1207").getAsInt();
        Assert.assertEquals(expectedEvents, actualEvents, 5);
    }
}